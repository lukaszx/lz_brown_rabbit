Brown Rabbit
============

This is the job test if you wish to be a web developer at HTML24

Contents
--------

When you have completed this test you will have demonstrated skills/knowledge in

* git
* html
* css
* js

Requirements
--------

Make sure to complete the following:

* Apply responsivness using media queries or freameworks for that 
* Make the search functional
* Make a custom js function for the slider 

Instructions
------------

To complete this test.

1. Fork this repository
2. Create a website based on the supplied PSD-file
3. Create a pull request to this repository